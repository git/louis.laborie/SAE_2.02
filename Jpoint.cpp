#include "Point.hpp"

Point :: Point(string nom) :
    nom{nom}{}

Point :: ~Point(){}

void Point :: ajouterLiaison(Point &p)
{
    list<Point> :: iterator it;
    for(it = liaisons.begin(); it != liaisons.end(); it++)
    {
        if(*it == p)
        {
            break;
        }
    }
    if(it == liaisons.end())
    {
        liaisons.push_back(p);
    }
    else
    {
        cout << this->nom << " possède déjà une liaison vers " << p.getNom() << endl;
    }
}

void Point :: supprimerLiaison(Point &p)
{
    list<Point> :: iterator it;
    for(it = liaisons.begin(); it != liaisons.end(); it++)
    {
        if(*it == p)
        {
            break;
        }
    }
    if(it != liaisons.end())
    {
        liaisons.remove(p);
    }
    else
    {
        cout << this->nom << " ne possède pas liaison vers " << p.getNom() << endl;
    }
}

string Point :: getNom() const
{
    return nom;
}

list<Point> Point :: getLiaisons() const
{
    return liaisons;
}

void Point :: afficheLiaisons() const {
    cout << nom << " :" << endl;
    for (const Point& p : liaisons) {
        cout << "-->" << p.getNom() << endl;
    }
    return ;
}

void Point :: afficheChemins() const
{
    vector<Point &> v;
    cout << nom << " :" << endl;
    for(Point &p : liaisons)
    {
        cout << "-->";
        if(find(v.begin(), v.end(), &p) != v.end())
        {
            p.afficheLiaisons();
        }
    }
    return ;
}

bool operator==(const Point &p1, const Point &p2)
{
    return p1.getNom() == p2.getNom();
}

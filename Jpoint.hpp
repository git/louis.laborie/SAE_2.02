#ifndef point
#define point

#include <vector>
#include <string>
#include <list>
#include <iostream>

using namespace std;

class Point{
    list<Point> liaisons;
    string nom;
public:
    Point(string nom);
    ~Point();
    void ajouterLiaison(Point &p);
    void supprimerLiaison(Point &p);
    string getNom() const;
    list<Point> getLiaisons() const;
    void afficheLiaisons() const;
    void afficheChemins() const;
    friend bool operator==(const Point &p1, const Point &p2);
};

#endif

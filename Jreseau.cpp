#include "Reseau.hpp"
#include "Point.hpp"

Reseau :: Reseau(){}

void Reseau :: ajouterRue(Point &p1, Point &p2)
{
    list<Point> :: iterator it1;
    list<Point> :: iterator it2;
    for(it1 = points.begin(); it1 != points.end(); it1++)
    {
        if(*it1 == p1)
        {
            break;
        }
    }
    for(it2 = points.begin(); it2 != points.end(); it2++)
    {
        if(*it2 == p2)
        {
            break;
        }
    }
    if(it1 != points.end() && it2 != points.end())
    {
        p1.ajouterLiaison(p2);
    }
}

void Reseau :: supprimerRue(Point &p1, Point &p2)
{
    list<Point> :: iterator it1;
    list<Point> :: iterator it2;
    for(it1 = points.begin(); it1 != points.end(); it1++)
    {
        if(*it1 == p1)
        {
            break;
        }
    }
    for(it2 = points.begin(); it2 != points.end(); it2++)
    {
        if(*it2 == p2)
        {
            break;
        }
    }
    if(it1 != points.end() && it2 != points.end())
    {
        p1.supprimerLiaison(p2);
    }
}

void Reseau::ajouterPoint(Point &p) {
    list<Point>::iterator it;
    for(it = points.begin() ; it != points.end() ; it++) {
        if (*it == p) break;
    }
    if (it != points.end()) {
        cout << "Le point existe déjà !" << endl;
        return ;
    }
    points.push_back(p);
}

void Reseau::supprimerPoint(Point &p) {
    list<Point>::iterator it;
    for(it = points.begin() ; it != points.end() ; it++) {
        if (*it == p) break;
    }
    if (it == points.end()) {
        cout << "Le point n'existe pas !" << endl;
        return ;
    }
    points.remove(p);
    for(Point &p1 : points) {
        this->supprimerRue(p1,p);
    }
}

void Reseau::affiche() const {
    for (const Point& p : points) {
        cout << p.getNom() << " ";
    }
    cout << endl;
}

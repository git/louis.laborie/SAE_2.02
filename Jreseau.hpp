#ifndef Jreseau
#define Jreseau

#include <vector>
#include <string>
#include <iostream>
#include <list>

using namespace std;

class Point;
class Reseau{
    list<Point> points;
public:
    Reseau();
    void ajouterRue(Point &p1, Point &p2);
    void supprimerRue(Point &p1, Point &p2);
    void ajouterPoint(Point &p);
    void supprimerPoint(Point &p);
    void affiche() const;
};

#endif

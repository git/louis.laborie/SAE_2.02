#include "Jpoint.hpp"
#include "Jreseau.hpp"
#include "Jrue.hpp"

int main(void) {
    Reseau r;
    Point p{"Rue de la Victoire"};
    Point c1{"Place Verlaine"};
    Point c2{"Rue Trotsky"};
    r.ajouterPoint(p);
    r.ajouterPoint(c1);
    r.ajouterPoint(c2);
    r.ajouterRue(p,c1);
    r.ajouterRue(p,c2);
    r.ajouterRue(c1,c2);
    r.ajouterRue(c1,p);
    r.ajouterRue(p,c1);
    r.affiche();
    p.afficheLiaisons();
    p.afficheChemins();
    return 0;
}
#include "point.hpp"

using namespace std;

Point::Point(string nom)
    :nom(nom) {}

string Point::getNom() const {
    return nom;
}

bool operator==(const Point & p1, const Point & p2) {
    return p1.nom == p2.nom;
}

void afficheLiaisons() const {
    cout << nom << " :" << endl;
    for (const Point& p : liaisons) {
        cout << "-->" << p.getNom() << endl;
    }
    return ;
}










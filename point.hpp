#include <vector>
#include <string>
#include <list>
#include <iostream>

class Point {
    std::list<Point> liaisons;
    std::string nom;
public:
    Point(std::string nom);
    std::string getNom() const;
    void afficheLiaisons() const;
    friend bool operator==(const Point & p1, const Point & p2);

};






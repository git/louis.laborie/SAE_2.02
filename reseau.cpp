#include "point.hpp"
#include "reseau.hpp"

using namespace std;

void Reseau::ajouterPoint(Point &p) {
    list<Point>::iterator it;
    for(it = points.begin() ; it != points.end() ; it++) {
        if (*it == p) break;
    }
    if (it != points.end()) {
        cout << "Le point existe déjà !" << endl;
        return ;
    }
    points.push_back(p);
}

void Reseau::supprimerPoint(Point &p) {
    list<Point>::iterator it;
    for(it = points.begin() ; it != points.end() ; it++) {
        if (*it == p) break;
    }
    if (it == points.end()) {
        cout << "Le point n'existe pas !" << endl;
        return ;
    }
    points.remove(p);
}

void Reseau::affiche() const {
    for (const Point& p : points) {
        cout << p.getNom() << " ";
    }
    cout << endl;
}

Reseau::Reseau() {}
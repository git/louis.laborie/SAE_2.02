#include <vector>
#include <string>
#include <list>
#include <iostream>

class Point;


class Reseau {
    std::list<Point> points;
public:
    Reseau();
    void ajouterPoint(Point &p);
    void supprimerPoint(Point &p);
    void affiche() const;
};
#include <stdio.h>



void echanger(int tab[], int i, int j) {
    int tmp;
    tmp = tab[i];
    tab[i] = tab[j];
    tab[j] = tmp;
}

void affiche(int tab[], int nb) {
    int i;
    for (i = 0 ; i < nb ; i++) {
        printf("%d ", tab[i]);
    }
}

void algo1(int tab[], int nb){
    int echange = 1;
    int i;
    int deb = 0, fin = nb -1;
    while (echange >0){
        echange = 0;
        for(i=deb; i < fin; i++){
            if(tab[i]> tab[i+1]){
                echanger(tab,i,i+1);
                echange ++;
            }
            affiche(tab,nb);
            printf(" | nombre d'échanges : %d\n",echange);
        }
        fin --;
        for(i=fin; i > deb; i--){
            if(tab[i]< tab[i-1]){
                echanger(tab,i,i-1);
                echange ++;
            }
            affiche(tab,nb);
            printf(" | nombre d'échanges : %d\n",echange);
        }
        deb ++;
    }
}

void algo2(int tab[], int nb){
    int* cpt;
    int min = tab[0] , max = tab[0];
    int i,j;
    for(i=1; i<nb; i++){
        if(min > tab[i]) min = tab[i];
        if(max < tab[i]) max = tab[i];
    }
    cpt = (int*)calloc((max-min +1) ,sizeof(int));
    if(cpt == NULL){
        printf("pb aloc mémoire \n");
        exit(1);
    }
    for(i = 0; i<nb; i++){
        cpt[tab[i]-min]++;
    }
    j=0;
    for(i=0; i< max -min +1 ; i++){
        while (cpt[i]!=0){
            tab[j] = i+min;
            j++;
            cpt[i]--;
        }
    }
    free(cpt);
}

int main(void) {
    int tab[10] = {5,1,0,9,10,3,8};
    int nb = 7;
    algo1(tab,nb);
    return 0;
}
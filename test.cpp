#include "point.hpp"
#include "reseau.hpp"

int main(void) {
    Point p{"Place Verlaine"};
    Point c1{"Rue de la Victoire"};
    Point c2{"Place Trotsky"};
    Reseau r1;
    r1.ajouterPoint(p);
    r1.ajouterPoint(c1);
    r1.affiche();
    r1.supprimerPoint(p);
    r1.affiche();
    return 0;
}